# Steps: Create Docusaurus Site

## 1. Create repo at gitlab (where we want to publish site)

## 2. clone repo: 
> `git@gitlab.com:sakib-rahman-bangladesh/test-site.git`

## 3. Go to project dir: 
> `cd test-site`

## 4. Create dir public: 
> `mkdir public`

## 5. Copy .yml + index.html + .sh files (from another vaild source) into necessary places to publish site

## 6. Edit .sh file. Change URL.git + commit text

## 7. Run .sh file. 
> sh `sakib-ci.sh`

### 404: The page you're looking for could not be found.
> Needs some times for live site... No worry, Please wait. You will see site soon.

## 8. Read first [Getting started](https://docusaurus.io/docs/en/installation) or Follow commands below
> install node + yarn (see from getting started)
> npx docusaurus-init

## 9. Rename
> ren docs-examples-from-docusaurus docs
> cd website
> ren blog-examples-from-docusaurus blog

## 10. Run locally
> `yarn run start` (If run successfull then follow steps below)

## 11. Change siteConfig.js
> Open test-site with a editor (atom/vscode/...etc what you like)
> Open siteConfig.js from website folder
> Make relevent changes

title: 'Test Site', // Title for your website.

  
tagline: 'A website for testing',
  

url: 'https://sakib-rahman-bangladesh.gitlab.io', // Your website URL
  

baseUrl: '/', // Base URL for your project */
  

// For github.io type URLs, you would set the url and baseUrl like:
  

//   url: 'https://facebook.github.io',
  

// baseUrl: '/test-site/',

  

// Used for publishing and more
  

projectName: 'test-site',
  

organizationName: 'Playlagom',

## 12. Build
> yarn run build (run this command inside website dir and you will get new build dir containing html generated files)

## 13. Replace public dir with the build/test-site/* res

## 14. Push 
> Edit commits at .sh file
> Run .sh file again 

See live docusaurus site. **Congratulations!**


